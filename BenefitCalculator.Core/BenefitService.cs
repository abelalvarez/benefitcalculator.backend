﻿using BenefitCalculator.Core.Discount;
using BenefitCalculator.Core.DTO;
using BenefitCalculator.Core.DTO.Overview;
using BenefitCalculator.Core.Utilities;
using BenefitCalculator.Database.Context;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("BenefitCalculator.Tests")]

namespace BenefitCalculator.Core
{
    public class BenefitService : IBenefitService
    {
        private readonly ILogger _logger;
        private readonly BenefitContext _context;
        private readonly IDiscountService _discountService;

        public BenefitService(ILogger<BenefitService> logger, BenefitContext context, IDiscountService discountService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _discountService = discountService ?? throw new ArgumentNullException(nameof(discountService));
        }

        public Overview QuoteBenefits(Employee employee)
        {
            _logger.LogInformation($"Starting processing Quote {employee.GetFullName()}");
            var occurrence = GetPaymentOccurrences(employee);

            var employeePreview = GetCustomerPreviewWithBenefits(employee, occurrence);
            var dependentPreview = employee.Dependents?.Select(x => GetCustomerPreviewWithBenefits(x, occurrence)).ToList();
            var deductions = employeePreview.Benefit.PaycheckCost + dependentPreview?.Sum(x => x.Benefit.PaycheckCost) ?? 0;
            var paycheck = GetPaycheck(employee, occurrence) - deductions;

            return new Overview
            {
                PayCheck = paycheck.RoundTo(),
                Deductions = deductions.RoundTo(),
                Employee = employeePreview.RoundTo(),
                Dependents = dependentPreview?.RoundTo()
            };
        }

        internal CustomerOverview GetCustomerPreviewWithBenefits(Person person, int occurrence)
        {
            var benefitCost = GetBenefitCost(person);
            var benefits = ApplyDiscountsAndCalculateBenefit(person, benefitCost, occurrence);

            return new CustomerOverview
            {
                FirstName = person.FirstName,
                LastName = person.LastName,
                MiddleName = person.MiddleName,
                FullName = person.GetFullName(),
                Benefit = benefits
            };
        }

        internal Benefit ApplyDiscountsAndCalculateBenefit(Person person, decimal totalCost, int occurrence)
        {
            Benefit result = _discountService.CalculateDiscounts(person, totalCost);

            result.AnnualCost = totalCost;
            result.PaycheckCost = CalculatePerPayCheck(result.AnnualCostAfterDiscounts, occurrence);

            return result;
        }

        private decimal CalculatePerPayCheck(decimal totalDue, int occurrence)
        {
            return totalDue / occurrence;
        }

        private decimal GetBenefitCost(Person person)
        {
            return _context.Benefits
                .Where(x => x.Type == person.GetType().Name)
                .Select(x => x.AnnualCost)
                .SingleOrDefault();
        }

        private decimal GetPaycheck(Employee employee, int occurrence)
        {
            var salary = GetSalary(employee);

            return salary / occurrence;
        }

        private decimal GetSalary(Employee employee) => 52000;// _context.Employees.FirstOrDefault().Salary;

        private int GetPaymentOccurrences(Employee employee) => 26;// _context.PaymentInfos.FirstOrDefault().Occurrence;
    }
}
