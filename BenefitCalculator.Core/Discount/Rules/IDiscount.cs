﻿using BenefitCalculator.Core.DTO;
using BenefitCalculator.Core.DTO.Overview;

namespace BenefitCalculator.Core.Discount.Rules
{
    public interface IDiscount
    {
        Benefit CalculateDiscount(Person person, decimal total);
    }
}
