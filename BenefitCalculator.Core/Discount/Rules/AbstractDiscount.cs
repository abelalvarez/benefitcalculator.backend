﻿using BenefitCalculator.Core.DTO;
using BenefitCalculator.Core.DTO.Overview;

namespace BenefitCalculator.Core.Discount.Rules
{
    public abstract class AbstractDiscount : IDiscount
    {
        internal readonly DiscountConfiguration _configuration;

        public AbstractDiscount(DiscountConfiguration configuration)
        {
            _configuration = configuration;
        }

        public abstract Benefit CalculateDiscount(Person person, decimal total);
    }
}
