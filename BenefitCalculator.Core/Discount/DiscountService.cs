﻿using BenefitCalculator.Core.Discount.Factories;
using BenefitCalculator.Core.Discount.Rules;
using BenefitCalculator.Core.DTO;
using BenefitCalculator.Core.DTO.Overview;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BenefitCalculator.Core.Discount
{
    public class DiscountService : IDiscountService
    {
        private readonly ILogger _logger;
        private readonly IDiscountFactory _discountFactory;
        private readonly IEnumerable<IDiscount> _discounts;


        public DiscountService(ILogger<DiscountService> logger, IDiscountFactory discountFactory)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _discountFactory = discountFactory ?? throw new ArgumentNullException(nameof(discountFactory));

            _discounts = _discountFactory
                .GetDiscounts()
                .ToList();
        }

        public Benefit CalculateDiscounts(Person person, decimal totalCost)
        {
            Benefit result = null;

            foreach (var discount in _discounts)
            {
                result = discount.CalculateDiscount(person, result?.AnnualCostAfterDiscounts ?? totalCost);
            }

            if (result == null)
            {
                result = new Benefit { AnnualCost = totalCost, AnnualCostAfterDiscounts = totalCost };
            }

            result.AnnualCost = totalCost;

            return result;
        }
    }
}
