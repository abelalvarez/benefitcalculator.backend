﻿using BenefitCalculator.Core.Discount.Rules;
using System.Collections.Generic;

namespace BenefitCalculator.Core.Discount.Factories
{
    public interface IDiscountFactory
    {
        IEnumerable<IDiscount> GetDiscounts();
    }
}
