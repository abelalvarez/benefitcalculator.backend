﻿using BenefitCalculator.Core.Discount.Rules;
using BenefitCalculator.Database.Context;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BenefitCalculator.Core.Discount.Factories
{
    public class DiscountFactory : IDiscountFactory
    {
        private readonly ILogger _logger;
        private readonly BenefitContext _context;

        public DiscountFactory(ILogger<DiscountFactory> logger, BenefitContext context)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IEnumerable<IDiscount> GetDiscounts()
        {
            return _context.Discounts
                .Where(x => x.Enabled)
                .ToList()
                .Select(x => GetDiscount(x));
        }

        private IDiscount GetDiscount(Database.Models.Discount discount)
        {
            var configuration = new DiscountConfiguration
            {
                Fixed = discount.FixedDiscount,
                Percent = discount.PercentDiscount,
                Parameter = discount.ConditionParameter,
            };

            switch (discount.ClassName)
            {
                case NameStartsWithDiscount.Name:
                    return new NameStartsWithDiscount(configuration);
                case LastNameEndsWithDiscount.Name:
                    return new LastNameEndsWithDiscount(configuration);
                default:
                    return null;
            }
        }
    }
}
