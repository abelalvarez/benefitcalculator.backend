﻿using BenefitCalculator.Core.DTO;
using BenefitCalculator.Core.DTO.Overview;

namespace BenefitCalculator.Core.Discount
{
    public interface IDiscountService
    {
        Benefit CalculateDiscounts(Person person, decimal totalCost);
    }
}
