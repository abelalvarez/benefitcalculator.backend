﻿using BenefitCalculator.Core.DTO.Overview;
using System.Collections.Generic;
using System.Linq;

namespace BenefitCalculator.Core.Utilities
{
    public static class Extensions
    {
        public static decimal RoundTo(this decimal value, int precision = 2)
        {
            return decimal.Round(value, precision);
        }

        public static CustomerOverview RoundTo(this CustomerOverview customer)
        {
            customer.Benefit.AnnualCost = customer.Benefit.AnnualCost.RoundTo();
            customer.Benefit.AnnualCostAfterDiscounts = customer.Benefit.AnnualCostAfterDiscounts.RoundTo();
            customer.Benefit.PaycheckCost = customer.Benefit.PaycheckCost.RoundTo();
            return customer;
        }

        public static IEnumerable<CustomerOverview> RoundTo(this IEnumerable<CustomerOverview> customers)
        {
            return customers.Select(x => x.RoundTo());
        }
    }
}
