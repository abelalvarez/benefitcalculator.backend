﻿using BenefitCalculator.Core.DTO;
using BenefitCalculator.Core.DTO.Overview;
namespace BenefitCalculator.Core
{
    public interface IBenefitService
    {
        Overview QuoteBenefits(Employee employee);
    }
}
