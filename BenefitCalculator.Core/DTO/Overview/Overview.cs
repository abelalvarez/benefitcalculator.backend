﻿using System.Collections.Generic;

namespace BenefitCalculator.Core.DTO.Overview
{
    public class Overview
    {
        public CustomerOverview Employee { get; set; }
        public IEnumerable<CustomerOverview> Dependents { get; set; }
        public decimal PayCheck { get; set; }
        public decimal Deductions { get; set; }
    }
}
