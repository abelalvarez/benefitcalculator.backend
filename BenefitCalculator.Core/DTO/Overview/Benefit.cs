﻿namespace BenefitCalculator.Core.DTO.Overview
{
    public class Benefit
    {
        public decimal AnnualCost { get; set; }
        public decimal AnnualCostAfterDiscounts{ get; set; }
        public decimal PaycheckCost{ get; set; }
    }
}
