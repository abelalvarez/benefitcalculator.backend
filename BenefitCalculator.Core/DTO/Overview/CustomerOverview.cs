﻿namespace BenefitCalculator.Core.DTO.Overview
{
    public class CustomerOverview : Person
    {
        public string FullName { get; set; }
        public Benefit Benefit { get; set; }
    }
}
