﻿using System.Collections.Generic;

namespace BenefitCalculator.Core.DTO
{
    public class Employee : Person
    {
        public List<Dependent> Dependents { get; set; }
    }
}
