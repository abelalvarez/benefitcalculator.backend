﻿namespace BenefitCalculator.Core.DTO
{
    public abstract class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string GetFullName()
        {
            var fullName = FirstName.Trim();

            if (!string.IsNullOrWhiteSpace(MiddleName))
            {
                fullName += " " + MiddleName.Trim();
            }

            fullName += " " + LastName.Trim();

            return fullName;
        }
    }
}
