﻿using BenefitCalculator.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace BenefitCalculator.Database.Context
{
    public class BenefitContext : DbContext
    {
        public BenefitContext() { }
        public BenefitContext(DbContextOptions<BenefitContext> options) : base(options) { }

        public DbSet<Benefit> Benefits { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<PaymentInfo> PaymentInfos { get; set; }
        public DbSet<Employee> Employees { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Benefit>(entity => {
                entity.Property(x => x.AnnualCost).HasColumnType("decimal(18,2)");
            });
            modelBuilder.Entity<Discount>(entity => {
                entity.Property(x => x.FixedDiscount).HasColumnType("decimal(18,2)");
                entity.Property(x => x.PercentDiscount).HasColumnType("decimal(5,2)");
            });
        }
    }
}
