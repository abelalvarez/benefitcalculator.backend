﻿using BenefitCalculator.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BenefitCalculator.Database.Context
{
    public static class DbInitializer
    {
        public static void Initialize(BenefitContext context)
        {
            if (context.Benefits.Any())
            {
                return;
            }

            var now = DateTime.Now;

            var benefits = new List<Benefit>
            {
                new Benefit
                {
                    Name = "CompanyBenefit",
                    Description = "Company benefit for employees",
                    Type = "Employee",
                    AnnualCost = 1000,
                    CreatedDate = now,
                    LastModified = now,

                },
                new Benefit
                {
                    Name = "CompanyBenefit",
                    Description = "Company benefit for dependent",
                    Type = "Dependent",
                    AnnualCost = 500,
                    CreatedDate = now,
                    LastModified = now,
                }
            };

            context.Benefits.AddRange(benefits);
            context.SaveChanges();

            var discounts = new List<Discount>
            {
                new Discount
                {
                    Name="NameStartsWith",
                    Description="Discount if name starts with",
                    ClassName="NameStartsWithDiscount",
                    ConditionParameter = "A",
                    CreatedDate = now,
                    LastModified = now,
                    Enabled = true,
                    FixedDiscount = 0,
                    PercentDiscount = 10
                },
                new Discount
                {
                    Name="NameEndsWith",
                    Description="Discount if name ends with",
                    ClassName="LastNameEndsWithDiscount",
                    ConditionParameter = "R",
                    CreatedDate = now,
                    LastModified = now,
                    Enabled = false,
                    FixedDiscount = 30,
                    PercentDiscount = 0
                }
            };

            context.Discounts.AddRange(discounts);
            context.SaveChanges();
        }
    }
}
