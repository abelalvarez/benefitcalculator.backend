﻿namespace BenefitCalculator.Database.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PayGroup { get; set; }
        public decimal Salary { get; set; } = 52000.00m;
    }
}
