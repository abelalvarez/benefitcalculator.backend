﻿using System;

namespace BenefitCalculator.Database.Models
{
    public class Discount
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ClassName { get; set; }
        public string ConditionParameter { get; set; }
        public decimal FixedDiscount { get; set; }
        public decimal PercentDiscount { get; set; }
        public bool Enabled { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModified { get; set; }
    }
}
