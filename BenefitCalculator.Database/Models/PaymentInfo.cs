﻿namespace BenefitCalculator.Database.Models
{
    public class PaymentInfo
    {
        public int Id { get; set; }
        public int Occurrence { get; set; } = 26;
    }
}
