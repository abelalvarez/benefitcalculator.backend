﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BenefitCalculator.Database.Models
{
    public class Benefit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public decimal AnnualCost { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModified { get; set; }
    }
}
