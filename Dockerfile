FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY . .

RUN dotnet restore BenefitCalculator.WebApi.sln --force
WORKDIR "/src/"
RUN dotnet publish "BenefitCalculator.WebApi/BenefitCalculator.WebApi.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=build ./app/publish .

ENTRYPOINT ["dotnet", "BenefitCalculator.WebApi.dll"]

