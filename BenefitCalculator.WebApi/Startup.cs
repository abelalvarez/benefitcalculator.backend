using BenefitCalculator.Core;
using BenefitCalculator.Core.Discount;
using BenefitCalculator.Core.Discount.Factories;
using BenefitCalculator.Database.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace BenefitCalculator.WebApi
{
    public class Startup
    {
        private const string MyPolicy = "Quote";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IBenefitService, BenefitService>();
            services.AddTransient<IDiscountService, DiscountService>();
            services.AddScoped<IDiscountFactory, DiscountFactory>();
            services.AddDbContext<BenefitContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("BenefitContext"), b => b.MigrationsAssembly("BenefitCalculator.WebApi")));

            services.AddCors(options =>
            {
                options.AddPolicy(MyPolicy, builder =>
                {
                    builder
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowAnyOrigin();
                });
            });

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "BenefitCalculator.WebApi", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "BenefitCalculator.WebApi v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(MyPolicy);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
