﻿using BenefitCalculator.Core;
using BenefitCalculator.Core.DTO;
using BenefitCalculator.Core.DTO.Overview;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace BenefitCalculator.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BenefitsController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IBenefitService _benefitService;

        public BenefitsController(ILogger<BenefitsController> logger, IBenefitService benefitService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _benefitService = benefitService ?? throw new ArgumentNullException(nameof(benefitService));
        }

        [HttpPost("quote")]
        public ActionResult<Overview> GetQuote(Employee employee)
        {
            try
            {
                return Created("quote", _benefitService.QuoteBenefits(employee));
            }
            catch (Exception ex)
            {
                _logger.LogError("Logging exception ", ex);
                throw;
            }
        }
    }
}
