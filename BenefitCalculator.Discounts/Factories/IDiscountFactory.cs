﻿using System.Collections.Generic;

namespace BenefitCalculator.Discounts.Factory
{
    public interface IDiscountFactory
    {
        IEnumerable<IDiscount> GetDiscounts();
    }
}
