﻿using BenefitCalculator.Database.Context;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BenefitCalculator.Core.Discount
{
    public class DiscountFactory : IDiscountFactory
    {
        private readonly ILogger _logger;
        private readonly BenefitContext _context;

        public DiscountFactory(ILogger<DiscountFactory> logger, BenefitContext context)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IEnumerable<IDiscount> GetDiscounts()
        {
            var discounts = _context.Discounts
                .Where(x => x.Enabled);

            foreach (var discount in discounts)
            {
                yield return GetDiscount(discount);
            }
        }

        private IDiscount GetDiscount(Database.Models.Discount discount)
        {
            var configuration = new DiscountConfiguration
            {
                Fixed = discount.FixedDiscount,
                Percent = discount.PercentDiscount,
                Parameter = discount.ConditionParameter,
            };

            switch (discount.ClassName)
            {
                case nameof(NameStartsWithDiscount):
                    return new NameStartsWithDiscount(configuration);
                case nameof(LastNameEndsWithDiscount):
                    return new LastNameEndsWithDiscount(configuration);
                default:
                    return null;
            }
        }
    }
}
