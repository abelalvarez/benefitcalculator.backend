﻿using BenefitCalculator.Core.DTO;
using BenefitCalculator.Core.DTO.Preview;

namespace BenefitCalculator.Discounts.Rules
{
    public interface IDiscount
    {
        Benefit CalculateDiscount(Person person, decimal total);
    }
}
