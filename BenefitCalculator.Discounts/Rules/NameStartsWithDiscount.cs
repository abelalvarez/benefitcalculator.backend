﻿using BenefitCalculator.Core.DTO;
using BenefitCalculator.Core.DTO.Preview;
using System;

namespace BenefitCalculator.Discounts.Rules
{
    public class NameStartsWithDiscount : AbstractDiscount
    {
        public NameStartsWithDiscount(DiscountConfiguration configuration) : base(configuration) { }
        public override Benefit CalculateDiscount(Person person, decimal total)
        {
            var newCost = total;

            if (person.FirstName.StartsWith(_configuration.Parameter, StringComparison.OrdinalIgnoreCase))
            {
                if (_configuration.Percent > 0)
                {
                    newCost = total - (total * _configuration.Percent) / 100;
                }
                else if (_configuration.Fixed > 0)
                {
                    newCost = total - _configuration.Fixed;
                }
            }

            return new Benefit
            {
                AnnualCost = total,
                AnnualCostAfterDiscounts = newCost,
            };
        }
    }
}
