﻿namespace BenefitCalculator.Discounts.Rules
{
    public class DiscountConfiguration
    {
        public decimal Fixed { get; set; }
        public decimal Percent { get; set; }
        public string Parameter { get; set; }
        public int Occurrences { get; set; }
    }
}
