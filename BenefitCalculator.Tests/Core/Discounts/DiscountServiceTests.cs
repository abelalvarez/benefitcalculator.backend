﻿using BenefitCalculator.Core.Discount;
using BenefitCalculator.Core.Discount.Factories;
using BenefitCalculator.Core.Discount.Rules;
using BenefitCalculator.Core.DTO;
using BenefitCalculator.Core.DTO.Overview;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace BenefitCalculator.Tests.Core.Discounts
{
    [TestClass]
    public class DiscountServiceTests
    {
        private readonly Mock<ILogger<DiscountService>> _loggerMock = new Mock<ILogger<DiscountService>>();
        private readonly Mock<IDiscountFactory> _discountFactory = new Mock<IDiscountFactory>();
        private Mock<LastNameEndsWithDiscount> _lastNameEndsWithDiscount = new Mock<LastNameEndsWithDiscount>(new DiscountConfiguration());
        private Mock<NameStartsWithDiscount> _nameStartsWithDiscount = new Mock<NameStartsWithDiscount>(new DiscountConfiguration());

        private DiscountService _discountService;

        public DiscountServiceTests()
        {
            _discountFactory.Setup(x => x.GetDiscounts())
                .Returns(new List<IDiscount> {
                    _nameStartsWithDiscount.Object,
                });

            _discountService = new DiscountService(_loggerMock.Object, _discountFactory.Object);
        }

        [TestMethod]
        public void CalculateDiscounts_Employee_NoDiscount()
        {
            var employee = new Employee
            {
                FirstName = "Abel",
                LastName = "Alvarez"
            };

            _nameStartsWithDiscount.Setup(x => x.CalculateDiscount(It.IsAny<Person>(), It.IsAny<decimal>()))
                .Returns(new Benefit
                {
                    AnnualCost = 1000,
                    AnnualCostAfterDiscounts = 1000,
                });
            var benefit = _discountService.CalculateDiscounts(employee, 1000);

            Assert.AreEqual(1000, benefit.AnnualCost);
            Assert.AreEqual(1000, benefit.AnnualCostAfterDiscounts);
        }

        [TestMethod]
        public void CalculateDiscounts_Employee_Discount()
        {
            var employee = new Employee
            {
                FirstName = "Abel",
                LastName = "Alvarez"
            };

            _nameStartsWithDiscount.Setup(x => x.CalculateDiscount(It.IsAny<Person>(), It.IsAny<decimal>()))
                .Returns(new Benefit
                {
                    AnnualCost = 1000,
                    AnnualCostAfterDiscounts = 900,
                });
            var benefit = _discountService.CalculateDiscounts(employee, 1000);

            Assert.AreEqual(1000, benefit.AnnualCost);
            Assert.AreEqual(900, benefit.AnnualCostAfterDiscounts);
        }

        [TestMethod]
        public void CalculateDiscounts_EmployeeNoDiscount_NoDiscount()
        {
            var employee = new Employee
            {
                FirstName = "Abel",
                LastName = "Alvarez"
            };

            _discountFactory.Setup(x => x.GetDiscounts())
                .Returns(new List<IDiscount>
                {
                });

            _discountService = new DiscountService(_loggerMock.Object, _discountFactory.Object);

            var benefit = _discountService.CalculateDiscounts(employee, 1000);

            Assert.AreEqual(1000, benefit.AnnualCost);
            Assert.AreEqual(1000, benefit.AnnualCostAfterDiscounts);
        }
    }
}
