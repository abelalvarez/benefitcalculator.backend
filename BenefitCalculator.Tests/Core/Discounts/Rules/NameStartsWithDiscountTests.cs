using BenefitCalculator.Core.Discount.Rules;
using BenefitCalculator.Core.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BenefitCalculator.Tests.Core.Discounts.Rules
{
    [TestClass]
    public class NameStartsWithDiscountTests
    {
        private NameStartsWithDiscount _nameStartsWithDiscount;

        [TestMethod]
        [DataRow(10, "A", 1000, 900)]
        [DataRow(10, "a", 1000, 900)]
        [DataRow(10, "aBe", 1000, 900)]
        [DataRow(10, "B", 1000, 1000)]
        [DataRow(10, "b", 1000, 1000)]
        public void CalculateDiscount_DifferentConfiguration(double percent, string parameter, double totalDue, double afterDiscounts)
        {
            var configuration = new DiscountConfiguration
            {
                Percent = (decimal)percent,
                Parameter = parameter
            };

            _nameStartsWithDiscount = new NameStartsWithDiscount(configuration);

            var employee = new Employee
            {
                FirstName = "Abel",
                LastName = "Alvarez",
            };

            var benefits = _nameStartsWithDiscount.CalculateDiscount(employee, (decimal)totalDue);

            Assert.AreEqual((decimal)afterDiscounts, benefits.AnnualCostAfterDiscounts);
        }
    }
}
