﻿using BenefitCalculator.Core;
using BenefitCalculator.Core.Discount;
using BenefitCalculator.Core.DTO;
using BenefitCalculator.Core.DTO.Overview;
using BenefitCalculator.Database.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BenefitCalculator.Tests.Core
{
    [TestClass]
    public class BenefitServiceTests
    {
        private readonly Mock<ILogger<BenefitService>> _loggerMock = new Mock<ILogger<BenefitService>>();
        private readonly Mock<IDiscountService> _discountServiceMock = new Mock<IDiscountService>();

        private BenefitContext _context;
        private BenefitService _benefitService;
        private int _occurrence = 26;

        public BenefitServiceTests()
        {
            var options = new DbContextOptionsBuilder<BenefitContext>()
              .UseInMemoryDatabase(databaseName: "BenefitsDB")
              .Options;

            _context = new BenefitContext(options);

            _benefitService = new BenefitService(_loggerMock.Object, _context, _discountServiceMock.Object);
        }

        #region Constructor Tests
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void BenefitService_NullLogger_ArgumentNullException()
        {
            _benefitService = new BenefitService(null, _context, _discountServiceMock.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void BenefitService_NullContext_ArgumentNullException()
        {
            _benefitService = new BenefitService(_loggerMock.Object, null, _discountServiceMock.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void BenefitService_NullDiscountFactory_ArgumentNullException()
        {
            _benefitService = new BenefitService(_loggerMock.Object, _context, null);
        }

        #endregion

        #region ApplyDiscountsAndCalculateBenefit Tests
        [TestMethod]
        public void ApplyDiscountsAndCalculateBenefit_WrongAnnualCost_UpdateAnnualCostCalculatePayment()
        {
            var employee = new Employee();
            var totalCost = 1000;

            _discountServiceMock
                .Setup(x => x.CalculateDiscounts(It.IsAny<Person>(), It.IsAny<decimal>()))
                .Returns(new Benefit { AnnualCost = 25, AnnualCostAfterDiscounts = totalCost });

            var benefit = _benefitService.ApplyDiscountsAndCalculateBenefit(employee, totalCost, _occurrence);

            Assert.AreEqual(totalCost, benefit.AnnualCost);
            Assert.AreEqual(totalCost, benefit.AnnualCostAfterDiscounts);
            Assert.AreEqual((decimal)totalCost / _occurrence, benefit.PaycheckCost);
        }

        [TestMethod]
        public void ApplyDiscountsAndCalculateBenefit_HasDiscount_UpdateAnnualCostCalculatePayment()
        {
            var employee = new Employee();
            var totalCost = 1000;

            _discountServiceMock
                .Setup(x => x.CalculateDiscounts(It.IsAny<Person>(), It.IsAny<decimal>()))
                .Returns(new Benefit { AnnualCost = 25, AnnualCostAfterDiscounts = totalCost - 100 });

            var benefit = _benefitService.ApplyDiscountsAndCalculateBenefit(employee, totalCost, _occurrence);

            Assert.AreEqual(totalCost, benefit.AnnualCost);
            Assert.AreEqual(totalCost - 100, benefit.AnnualCostAfterDiscounts);
            Assert.AreEqual((decimal)(totalCost - 100) / _occurrence, benefit.PaycheckCost);
        }

        #endregion

        #region GetCustomerPreviewWithBenefits Tests

        [TestMethod]
        public void GetCustomerPreviewWithBenefits_EmployeeOneDiscounts_CustomerPreview()
        {
            var employee = new Employee
            {
                FirstName = "Abel",
                LastName = "Alvarez"
            };

            var benefitCost = 1000;
            Setup(benefitCost);

            var customerPreview = _benefitService.GetCustomerPreviewWithBenefits(employee, _occurrence);

            Assert.AreEqual("Abel Alvarez", customerPreview.FullName);
            Assert.AreEqual(benefitCost, customerPreview.Benefit.AnnualCost);
            Assert.AreEqual(benefitCost - 100, customerPreview.Benefit.AnnualCostAfterDiscounts);
            Assert.AreEqual((decimal)(benefitCost - 100) / _occurrence, customerPreview.Benefit.PaycheckCost);
        }

        [TestMethod]
        public void GetCustomerPreviewWithBenefits_DependantNoDiscounts_CustomerPreview()
        {
            var dependent = new Dependent
            {
                FirstName = "Carlos",
                LastName = "Alvarez"
            };

            var benefitCost = 500;
            Setup(employeeBenefitCost: benefitCost, applyEmployeeDiscount: false, applyDependentDiscount: false);

            var customerPreview = _benefitService.GetCustomerPreviewWithBenefits(dependent, _occurrence);

            Assert.AreEqual("Carlos Alvarez", customerPreview.FullName);
            Assert.AreEqual(benefitCost, customerPreview.Benefit.AnnualCost);
            Assert.AreEqual(benefitCost, customerPreview.Benefit.AnnualCostAfterDiscounts);
            Assert.AreEqual((decimal)(benefitCost) / _occurrence, customerPreview.Benefit.PaycheckCost);
        }
        #endregion

        #region QuoteBenefits Tests

        [TestMethod]
        public void QuoteBenefits_EmployeeOnly_NoDiscount()
        {
            var employee = new Employee
            {
                FirstName = "Abel",
                LastName = "Alvarez",
                Dependents = new List<Dependent> { }
            };

            Setup(1000, false);

            var preview = _benefitService.QuoteBenefits(employee);

            Assert.AreEqual("Abel Alvarez", preview.Employee.FullName);
            Assert.AreEqual(1000, preview.Employee.Benefit.AnnualCost);
            Assert.AreEqual(1000, preview.Employee.Benefit.AnnualCostAfterDiscounts);
            Assert.AreEqual(38.46m, preview.Employee.Benefit.PaycheckCost);

            Assert.AreEqual(1961.54m, preview.PayCheck);
            Assert.AreEqual(38.46m, preview.Deductions);
        }

        [TestMethod]
        public void QuoteBenefits_EmployeeOnly_DiscountApplied()
        {
            var employee = new Employee
            {
                FirstName = "Abel",
                LastName = "Alvarez",
                Dependents = new List<Dependent> { }
            };

            Setup(1000);

            var preview = _benefitService.QuoteBenefits(employee);

            Assert.AreEqual("Abel Alvarez", preview.Employee.FullName);
            Assert.AreEqual(1000, preview.Employee.Benefit.AnnualCost);
            Assert.AreEqual(900, preview.Employee.Benefit.AnnualCostAfterDiscounts);
            Assert.AreEqual(34.62m, preview.Employee.Benefit.PaycheckCost);

            Assert.AreEqual(1965.38m, preview.PayCheck);
            Assert.AreEqual(34.62m, preview.Deductions);
        }

        [TestMethod]
        public void QuoteBenefits_EmployeeAndDependent_DiscountAppliedEmployee()
        {
            var dependent = new Dependent
            {
                FirstName = "Carlos",
                LastName = "Alvarez"
            };
            var employee = new Employee
            {
                FirstName = "Abel",
                LastName = "Alvarez",
                Dependents = new List<Dependent> { dependent }
            };

            Setup(employeeBenefitCost: 1000, applyEmployeeDiscount: true, dependentBenefitCost: 500, applyDependentDiscount: false);

            var preview = _benefitService.QuoteBenefits(employee);

            Assert.AreEqual("Carlos Alvarez", preview.Dependents.First().FullName);
            Assert.AreEqual(500, preview.Dependents.First().Benefit.AnnualCost);
            Assert.AreEqual(500, preview.Dependents.First().Benefit.AnnualCostAfterDiscounts);
            Assert.AreEqual(19.23m, preview.Dependents.First().Benefit.PaycheckCost);

            Assert.AreEqual("Abel Alvarez", preview.Employee.FullName);
            Assert.AreEqual(1000, preview.Employee.Benefit.AnnualCost);
            Assert.AreEqual(900, preview.Employee.Benefit.AnnualCostAfterDiscounts);
            Assert.AreEqual(34.62m, preview.Employee.Benefit.PaycheckCost);

            Assert.AreEqual(1946.15m, preview.PayCheck);
            Assert.AreEqual(53.85m, preview.Deductions);
        }

        [TestMethod]
        public void QuoteBenefits_EmployeeAndDependent_DiscountAppliedEmployeeAndDependent()
        {
            var dependent = new Dependent
            {
                FirstName = "Carlos",
                LastName = "Alvarez"
            };
            var employee = new Employee
            {
                FirstName = "Abel",
                LastName = "Alvarez",
                Dependents = new List<Dependent> { dependent }
            };

            Setup(employeeBenefitCost: 1000, applyEmployeeDiscount: true, dependentBenefitCost: 500, applyDependentDiscount: true);

            var preview = _benefitService.QuoteBenefits(employee);

            Assert.AreEqual("Carlos Alvarez", preview.Dependents.First().FullName);
            Assert.AreEqual(500, preview.Dependents.First().Benefit.AnnualCost);
            Assert.AreEqual(400, preview.Dependents.First().Benefit.AnnualCostAfterDiscounts);
            Assert.AreEqual(15.38m, preview.Dependents.First().Benefit.PaycheckCost);

            Assert.AreEqual("Abel Alvarez", preview.Employee.FullName);
            Assert.AreEqual(1000, preview.Employee.Benefit.AnnualCost);
            Assert.AreEqual(900, preview.Employee.Benefit.AnnualCostAfterDiscounts);
            Assert.AreEqual(34.62m, preview.Employee.Benefit.PaycheckCost);

            Assert.AreEqual(1950m, preview.PayCheck);
            Assert.AreEqual(50m, preview.Deductions);
        }
        #endregion

        public void Setup(decimal employeeBenefitCost = 1000, bool applyEmployeeDiscount = true, decimal dependentBenefitCost = 500, bool applyDependentDiscount = true)
        {
            decimal employeeDiscount = applyEmployeeDiscount ? 100 : 0;
            decimal dependentDiscount = applyDependentDiscount ? 100 : 0;

            _discountServiceMock
               .Setup(x => x.CalculateDiscounts(It.IsAny<Employee>(), It.IsAny<decimal>()))
               .Returns(new Benefit { AnnualCost = employeeBenefitCost, AnnualCostAfterDiscounts = employeeBenefitCost - employeeDiscount });

            _discountServiceMock
               .Setup(x => x.CalculateDiscounts(It.IsAny<Dependent>(), It.IsAny<decimal>()))
               .Returns(new Benefit { AnnualCost = dependentBenefitCost, AnnualCostAfterDiscounts = dependentBenefitCost - dependentDiscount });

            if (!_context.Benefits.Any())
            {
                _context.Benefits.AddRange(new List<Database.Models.Benefit>{
                new Database.Models.Benefit
                {
                    Name = "CompanyBenefit",
                    Description = "Company benefit for employees",
                    Type = "Employee",
                    AnnualCost = 1000,
                    CreatedDate = DateTime.Now,
                    LastModified = DateTime.Now,
                },
                new Database.Models.Benefit
                {
                    Name = "CompanyBenefit",
                    Description = "Company benefit for dependent",
                    Type = "Dependent",
                    AnnualCost = 500,
                    CreatedDate = DateTime.Now,
                    LastModified = DateTime.Now,
                }
            });
                _context.SaveChanges();
            }
        }
    }
}
