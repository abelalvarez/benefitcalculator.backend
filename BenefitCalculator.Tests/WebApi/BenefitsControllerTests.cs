﻿using BenefitCalculator.Core;
using BenefitCalculator.Core.DTO;
using BenefitCalculator.Core.DTO.Overview;
using BenefitCalculator.WebApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace BenefitCalculator.Tests.WebApi
{
    [TestClass]
    public class BenefitsControllerTests
    {
        private readonly Mock<IBenefitService> _benefitServiceMock = new Mock<IBenefitService>();
        private readonly Mock<ILogger<BenefitsController>> _loggerMock = new Mock<ILogger<BenefitsController>>();
        private BenefitsController _controller; 
        
        public BenefitsControllerTests()
        {
            _controller = new BenefitsController(_loggerMock.Object, _benefitServiceMock.Object);
        }

        #region Constructor Tests
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void BenefitsController_NullLogger_ArgumentNullException()
        {
            _controller = new BenefitsController(null, _benefitServiceMock.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void BenefitsController_NullBenefitService_ArgumentNullException()
        {
            _controller = new BenefitsController(_loggerMock.Object, null);
        }
        #endregion

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void BenefitsController_BadParameter_Exception()
        {
            _benefitServiceMock
                .Setup(x => x.QuoteBenefits(It.Is<Employee>(i => i == null)))
                .Throws(new Exception("Testing Exception"));
            
            _controller.GetQuote(null);
        }

        [TestMethod]
        public void BenefitsController_ValidParameter_Created()
        {
            _benefitServiceMock
                .Setup(x => x.QuoteBenefits(It.IsAny<Employee>()))
                .Returns(new Overview());

            var response = _controller.GetQuote(new Employee());
            var result = response.Result as CreatedResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("quote", result.Location);
        }
    }
}
